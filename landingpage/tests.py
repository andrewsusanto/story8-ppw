from django.test import TestCase, Client, LiveServerTestCase
from django.http import HttpRequest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

import os
import time

from .views import index
from faker import Faker



# Create your tests here.

class LandingPageUnitTest(TestCase):
    def test_landing_page_found(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

class LandingPageFunctionalTest(LiveServerTestCase):
    def setUp(self):
        super(LandingPageFunctionalTest, self).setUp()

        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument("--headless")
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('disable-gpu')

        if 'GITLAB_CI' in os.environ:
            self.browser = webdriver.Chrome(
                './chromedriver', chrome_options=chrome_options)
        else:
            self.browser = webdriver.Chrome(chrome_options=chrome_options)        

    def tearDown(self):
        self.browser.quit()

        super(LandingPageFunctionalTest, self).tearDown()

    def test_check_accordion_title_requirement(self):
        requirements = ['my activity', 'organizational experience', 'achievement']

        self.browser.get(self.live_server_url + '/')

        for requirement in requirements:
            self.assertIn(requirement, self.browser.page_source.lower())

    def test_check_accordion_click_show_data(self):
        acitvity_keyword = ['Study', 'NEW THINGS', 'Experiment']
        organizational_experience_keyword = ['PERAK', 'BEM', 'Council']
        achievement_keyword = ['Science', 'School']
        contact_me_keyword = ['susa********************', '0858837*****']

        self.browser.get(self.live_server_url + '/')
        time.sleep(5)
        self.browser.execute_script("window.scrollTo(0, document.body.scrollHeight);")

        activity_button = self.browser.find_element_by_xpath("//div[@class='accordion-header-left'][contains(.,'My activity')][1]")
        activity_button.click()

        xpath_statement = "//div[@class='accordion-body']"

        for keyword in acitvity_keyword:
            xpath_statement += "[contains(.,'"+keyword+"')]"

        xpath_statement += "[1]"

        activity_body = self.browser.find_element_by_xpath(xpath_statement)
        self.assertTrue(activity_body.is_displayed())

        organizational_button = self.browser.find_element_by_xpath("//div[@class='accordion-header-left'][contains(.,'Organizational experience')][1]")
        organizational_button.click()
        time.sleep(5)

        xpath_statement = "//div[@class='accordion-body']"

        for keyword in organizational_experience_keyword:
            xpath_statement += "[contains(.,'"+keyword+"')]"

        xpath_statement += "[1]"

        organizational_body = self.browser.find_element_by_xpath(xpath_statement)
        self.assertTrue(organizational_body.is_displayed())

        achievement_button = self.browser.find_element_by_xpath("//div[@class='accordion-header-left'][contains(.,'Achievement')][1]")
        achievement_button.click()
        time.sleep(5)

        xpath_statement = "//div[@class='accordion-body']"

        for keyword in achievement_keyword:
            xpath_statement += "[contains(.,'"+keyword+"')]"

        xpath_statement += "[1]"

        achievement_body = self.browser.find_element_by_xpath(xpath_statement)
        self.assertTrue(achievement_body.is_displayed())

        achievement_button = self.browser.find_element_by_xpath("//div[@class='accordion-header-left'][contains(.,'Contact Me')][1]")
        achievement_button.click()
        time.sleep(5)

        xpath_statement = "//div[@class='accordion-body']"

        for keyword in contact_me_keyword:
            xpath_statement += "[contains(.,'"+keyword+"')]"

        xpath_statement += "[1]"

        contact_me_body = self.browser.find_element_by_xpath(xpath_statement)
        self.assertTrue(contact_me_body.is_displayed())

    def test_up_and_down_accordion(self):
        self.browser.get(self.live_server_url + '/')
        time.sleep(5)
        self.browser.execute_script("window.scrollTo(0, 325);")
        time.sleep(5)
        
        up_activity_button = self.browser.find_element_by_xpath("//div[@class='accordion shadow aos-init aos-animate'][contains(.,'My activity')]/div[1]/div[2]/div[1]")
        down_activity_button = self.browser.find_element_by_xpath("//div[@class='accordion shadow aos-init aos-animate'][contains(.,'My activity')]/div[1]/div[2]/div[2]")

        page_source = self.browser.page_source

        self.assertTrue(page_source.find('My activity') < page_source.find('Organizational experience'))

        down_activity_button.click()
        page_source = self.browser.page_source

        self.assertTrue(page_source.find('My activity') > page_source.find('Organizational experience'))

        up_organizational_button = self.browser.find_element_by_xpath("//div[@class='accordion shadow aos-init aos-animate'][contains(.,'Organizational experience')]/div[1]/div[2]/div[1]")
        down_organizational_button = self.browser.find_element_by_xpath("//div[@class='accordion shadow aos-init aos-animate'][contains(.,'Organizational experience')]/div[1]/div[2]/div[2]")

        down_organizational_button.click()
        page_source = self.browser.page_source

        self.assertTrue(page_source.find('My activity') < page_source.find('Organizational experience'))

    def test_change_theme_button(self):
        self.browser.get(self.live_server_url + '/')
        time.sleep(10)
        self.browser.execute_script("window.scrollTo(0, 100);")

        accordion = self.browser.find_element_by_xpath("//div[@class='accordion shadow aos-init aos-animate'][1]")
        accordion_color_before = accordion.value_of_css_property("background-color")
        accordion_background_before = accordion.value_of_css_property("background")

        background_image_before = self.browser.find_element_by_xpath("//img[@id='backgroundImage1']")
        display_background_image_before = background_image_before.value_of_css_property("display")
        
        change_theme_button = self.browser.find_element_by_xpath("//button[@id='changeTheme']")
        change_theme_button.click()

        accordion = self.browser.find_element_by_xpath("//div[@class='accordion shadow aos-init aos-animate'][1]")
        accordion_color_after = accordion.value_of_css_property("background-color")
        accordion_background_after = accordion.value_of_css_property("background")

        background_image_after = self.browser.find_element_by_xpath("//img[@id='backgroundImage1']")
        display_background_image_after = background_image_after.value_of_css_property("display")

        background_image_next = self.browser.find_element_by_xpath("//img[@id='backgroundImage2']")
        display_background_image_next = background_image_next.value_of_css_property("display")

        self.assertTrue(accordion_color_before != accordion_color_after or accordion_background_before != accordion_background_after)
        self.assertTrue(display_background_image_before != display_background_image_after)
        self.assertTrue(display_background_image_next != 'none')

        