# story8-ppw

Story 8 PPW Fasilkom UI Tahun Pelajaran 2019/2020 Semester Genap

# Biodata diri

Name    : Andrew

NPM     : 1906350692

Class   : PPW

Deployed at : https://story8ppw-andrew.herokuapp.com/

[![pipeline status](https://gitlab.com/andrewsusanto/story8-ppw/badges/master/pipeline.svg)](https://gitlab.com/andrewsusanto/story8-ppw/-/commits/master)
[![coverage report](https://gitlab.com/andrewsusanto/story8-ppw/badges/master/coverage.svg)](https://gitlab.com/andrewsusanto/story8-ppw/-/commits/master)
